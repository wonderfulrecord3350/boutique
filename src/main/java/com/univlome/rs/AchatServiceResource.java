/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.univlome.rs;

import com.univlome.entites.Achat;
import com.univlome.service.AchatService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Wonderful
 */
@Path("/achatService")
public class AchatServiceResource {
    AchatService as =new AchatService();
    
    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public  void addAchat(Achat a){
        as.ajouter(a);
          
    }
    
    @POST
    @Path("/update")
    @Produces("MediaType.APPLICATION_JSON")
    public void  updateAchat(Achat a){
        as.modifier(a);
        
    }
    @GET
    @Path("/find")
    @Produces("MediaType.APPLICATION_JSON")
    public Achat findAchat(@PathParam("id")int id){
        return as.trouver(id);
        
    }
    
    @DELETE
    @Path("/deleteAvecId")
    public void deleteAchat(@PathParam("id")int id){
        as.supprimer(id);
        
    }
    
    @DELETE
    @Path("/delete")
    public void  deleteAchat(Achat a){
        as.supprimer(a);
        
    }
    
    @GET
    @Path("/listerSimple")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Achat> getListe(){
        return as.lister();
    }
    
    @GET
    @Path("/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Achat> getListe(@PathParam("debut")int debut,@PathParam("nombre") int nombre){
        return as.lister(debut, nombre);
    }
    
    
}
