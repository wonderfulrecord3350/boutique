/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.univlome.rs;

import com.univlome.entites.Client;
import com.univlome.service.ClientService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Wonderful
 */
@Path("/clientService")
public class ClientServiceResource {
    ClientService cs =new ClientService();
    
    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void  addCategorie(Client e){
        cs.ajouter(e);
          
    }
    
    @POST
    @Path("update")
    public void updateClient(Client e){
        cs.modifier(e);
        
    }
    @GET
    @Path("/find")
    public Client findClient(@PathParam("id") int id){
        return cs.trouver(id);
        
    }
    
    @DELETE
    @Path("/deleteAvecId")
    public void  deleteClient(@PathParam("id") int id){
        cs.supprimer(id);
        
    }
    
    @DELETE
    @Path("/delete")
    public void  deleteClient(Client e){
        cs.supprimer(e);
        
    }
    
    @GET
    @Path("/listerSimple")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Client> getListe(){
        return cs.lister();
    }
    
    @GET
    @Path("/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Client> getListe(@PathParam("debut") int debut,@PathParam("nombre") int nombre){
        return cs.lister(debut, nombre);
    }
    
    
}
