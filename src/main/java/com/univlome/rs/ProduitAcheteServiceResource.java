/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.univlome.rs;

import com.univlome.entites.ProduitAchete;
import com.univlome.service.ProduitAcheteService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Wonderful
 */
@Path("/produitAcheteService")
public class ProduitAcheteServiceResource {
    ProduitAcheteService pas =new ProduitAcheteService();
    
    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addCategorie(ProduitAchete pa){
        pas.ajouter(pa);
          
    }
    
    @POST
    @Path("/update")
    public void updateProduitAchete(ProduitAchete e){
        pas.modifier(e);
        
    }
    @GET
    @Path("/find")
    @Produces("MediaType.APPLICATION_JSON")
    public ProduitAchete findProduitAchete(@PathParam("id") int id){
       return pas.trouver(id);
        
    }
    
    @DELETE
    @Path("/deleteAvecId")
    public void deleteProduitAchete(@PathParam("id") int id){
        pas.supprimer(id);
        
    }
    
    @DELETE
    @Path("/delete")
    public void deleteProduitAchete(ProduitAchete e){
        pas.supprimer(e);
        
    }
    
    @GET
    @Path("/listerSimple")
    @Produces("MediaType.APPLICATION_JSON")
    public List<ProduitAchete> getListe(){
        return pas.lister();
    }
    
    @GET
    @Path("/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<ProduitAchete> getListe(@PathParam("debut") int debut,@PathParam("nombre") int nombre){
        return pas.lister(debut, nombre);
    }
    
    
}
