/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.univlome.service;

import com.univlome.entites.Personne;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.Path;

/**
 *
 * @author Wonderful
 */
@Path("/boutique")
public class PersonneService {
      private List<Personne> liste;
    
    public void ajouter(Personne p){
         if(liste.contains(p))
            System.out.println("l'élement existe déja ");
        else
            liste.add(p); 
        
    }
    
    public void modifier(Personne p){
        if(liste.contains(p)){
            for(Personne c :liste){
                if(c.equals(p))
                    c=p;
            }
        }
        else 
            System.out.println("Id introuvable");
        
    }
    
    public Personne trouver(int id){
         Personne ctemp =new Personne();
        if(liste==null)
            return null;
        else{
            for(Personne c : liste){
                if(c.getId()==id){
                   System.out.println("Categorie trouvée");
                   ctemp=c;   
                } 
                else 
                    System.out.println("Id introuvable dans la liste");
            }
            return ctemp;
        }
       
        
    }
    
    public void supprimer(int id){
        if(trouver(id)!=null)
            liste.remove(trouver(id));
        
    }
    
    public void supprimer(Personne p){
         if(liste.contains(p))
            liste.remove(p);
        else
            System.out.println("cette categorie n'exite pas dans la liste");
        
    }
    
    public List<Personne> lister(){
         return new ArrayList<>((Collection<? extends Personne>) liste.iterator());
       
        
    }
    
    public List<Personne> lister(int debut, int nombre){
          int index =debut;
        List<Personne> maliste = null;
        if(liste ==null){
            System.out.println("La vide est vide ");
            return null;
        }
        else{
            if(debut >liste.size() || nombre >liste.size())
                System.out.println("La taile de la liste est debordée");
            else{
                if(index==debut){
                      do{
                    maliste.add(liste.get(index));
                    index++;
                    nombre--;    
                }while(nombre==0);
                    
                }
              
            }
        }
         
      return new ArrayList<>((Collection<? extends Personne>) maliste.iterator());
        
    }
       
        
    }
    

