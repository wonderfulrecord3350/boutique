/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.univlome.service;

import com.univlome.entites.Produit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.Path;

/**
 *
 * @author Wonderful
 */
@Path("/boutique")
public class ProduitService {
      private List<Produit> liste;
    
    public void ajouter(Produit p){
        if(liste.contains(p))
            System.out.println("l'élement existe déja ");
        else
            liste.add(p); 
        
    }
    
    public void modifier(Produit p){
        if(liste.contains(p)){
            for(Produit c :liste){
                if(c.equals(p))
                    c=p;
            }
        }
        else 
            System.out.println("Id introuvable");
        
    }
    
    public Produit trouver(int id){
          Produit ptemp =new Produit();
        if(liste==null)
            return null;
        else{
            for(Produit c : liste){
                if(c.getId()==id){
                   System.out.println("Categorie trouvée");
                   ptemp=c;   
                } 
                else 
                    System.out.println("Id introuvable dans la liste");
            }
            return ptemp;
        }
        
        
    }
    
    public void supprimer(int id){
        if(trouver(id)!=null)
            liste.remove(trouver(id));
        
    }
    
    public void supprimer(Produit p){
        if(liste.contains(p))
            liste.remove(p);
        else
            System.out.println("cette categorie n'exite pas dans la liste");
        
    }
    
    public List<Produit> lister(){
        return new ArrayList<>((Collection<? extends Produit>) liste.iterator());
        
    }
    
    public List<Produit> lister(int debut, int nombre){
        int index =debut;
        List<Produit> maliste = null;
        if(liste ==null){
            System.out.println("La vide est vide ");
            return null;
        }
        else{
            if(debut >liste.size() || nombre >liste.size())
                System.out.println("La taile de la liste est debordée");
            else{
                if(index==debut){
                      do{
                    maliste.add(liste.get(index));
                    index++;
                    nombre--;    
                }while(nombre==0);
                    
                }
              
            }
        }
         
      return new ArrayList<>((Collection<? extends Produit>) maliste.iterator());
        
    }
        
    }
    

