/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.univlome.service;

import com.univlome.entites.ProduitAchete;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.Path;

/**
 *
 * @author Wonderful
 */
@Path("/boutique")
public class ProduitAcheteService {
    private List<ProduitAchete> liste;
    
    public void ajouter(ProduitAchete pa){
         if(liste.contains(pa))
            System.out.println("l'élement existe déja ");
        else
            liste.add(pa); 
        
    }
    
    public void modifier(ProduitAchete pa){
        if(liste.contains(pa)){
            for(ProduitAchete c :liste){
                if(c.equals(pa))
                    c=pa;
            }
        }
        else 
            System.out.println("Id introuvable");
        
    }
    
    public ProduitAchete trouver(int id){
        return null;
        //ProduitAchete n'a pas d'id pour faire une recherche en se basant sur cet attribut
         
    }
    
    public void supprimer(int id){
        if(trouver(id)!=null)
            liste.remove(trouver(id));
        
    }
    
    public void supprimer(ProduitAchete pa){
         if(liste.contains(pa))
            liste.remove(pa);
        else
            System.out.println("cette categorie n'exite pas dans la liste");
        
    }
    
    public List<ProduitAchete> lister(){
         return new ArrayList<>((Collection<? extends ProduitAchete>) liste.iterator());
       
        
    }
    
    public List<ProduitAchete> lister(int debut, int nombre){
          int index =debut;
        List<ProduitAchete> maliste = null;
        if(liste ==null){
            System.out.println("La vide est vide ");
            return null;
        }
        else{
            if(debut >liste.size() || nombre >liste.size())
                System.out.println("La taile de la liste est debordée");
            else{
                if(index==debut){
                      do{
                    maliste.add(liste.get(index));
                    index++;
                    nombre--;    
                }while(nombre==0);
                    
                }
              
            }
        }
         
      return new ArrayList<>((Collection<? extends ProduitAchete>) maliste.iterator());
        
    }
        
        
    }
    

